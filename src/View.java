package tema1.tema1;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class View {

	public JFrame startFrame;
	public JLabel polL,pol2L, sumL, prodL, difL, rezL, rezultat;
	public JTextField polTxt,pol2Txt;
	public JButton sumB, difB, prodB, clrBtn, deriv1, deriv2;
	
	public View() {
		
		startFrame = new JFrame("Calculator Polinoame");
		startFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		startFrame.setLayout(new BorderLayout());
		startFrame.setSize(500, 200);
		JPanel up = new JPanel();
		up.setLayout(new GridLayout(2,1));
		JPanel pol1 = new JPanel();
		polL = new JLabel("Polinom 1: ");
		polTxt = new JTextField(30);
		deriv1 = new JButton("Deriv");
		pol1.add(polL);
		pol1.add(polTxt);
		pol1.add(deriv1);
		JPanel pol2 = new JPanel();
		pol2L = new JLabel("Polinom 2: ");
		pol2Txt = new JTextField(30);
		deriv2 = new JButton("Deriv");
		pol2.add(pol2L);
		pol2.add(pol2Txt);
		pol2.add(deriv2);
		up.add(pol1);
		up.add(pol2);
		JPanel center = new JPanel();
		rezL = new JLabel("Rezultat: ");
		rezultat = new JLabel("x");
		center.add(rezL);
		center.add(rezultat);
		JPanel btns = new JPanel();
		sumB = new JButton("Sum");
		difB = new JButton("Dif");
		prodB = new JButton("Prod");
		clrBtn = new JButton("CL");
		btns.add(sumB);
		btns.add(difB);
		btns.add(prodB);
		btns.add(clrBtn);
		startFrame.add(up, BorderLayout.NORTH);
		startFrame.add(center, BorderLayout.CENTER);
		startFrame.add(btns, BorderLayout.SOUTH);
		startFrame.setVisible(true);	
	}
	public void addSumList(ActionListener listener){
		sumB.addActionListener(listener);
	}
	public void clear(ActionListener listener){
		clrBtn.addActionListener(listener);
	}
	public void difSumList(ActionListener listener) {
		difB.addActionListener(listener);
	}
	public void prodSumList(ActionListener listener) {
		prodB.addActionListener(listener);
	}
	public void deriv1List(ActionListener listener) {
		deriv1.addActionListener(listener);
	}
	public void deriv2List(ActionListener listener) {
		deriv2.addActionListener(listener);
	}
}
