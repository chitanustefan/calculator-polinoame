package tema1.tema1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class Polinom {

	public ArrayList<Monom> monom;
	
	public Polinom(ArrayList<Monom> monom) {
		this.monom = monom;
	}
	
	public String afisare() {
		String pol = "";
		for(Monom m: this.monom){
			if(m.getCoef() >= 0)
				pol += "+" + m.getMonom();
			else pol += m.getMonom();
		}
		return pol;
	}
	
	
	public Polinom prod(Polinom p) {
		ArrayList<Monom> monom1 = new ArrayList<Monom>();
		for(Monom m: this.monom) {
			for(Monom m2: p.monom) {
				int coef = m.getCoef() * m2.getCoef();
				int putere = m.getPutere() + m2.getPutere();
				Monom m3 = new Monom(coef, putere);
				monom1.add(m3);
			}
		}
		
		Polinom prod = new Polinom(monom1);
		return prod;
	}
	
	public void merge() {
		ArrayList<Monom> monom1 = new ArrayList<Monom>();
		Collections.sort(this.monom);	
		for(int i=0; i<this.monom.size(); i++) {
			if(this.monom.get(i) != null) {
				if(i!=this.monom.size()-1) {
					if(this.monom.get(i).getPutere() == this.monom.get(i+1).getPutere()) {
						int coef = this.monom.get(i).getCoef() + this.monom.get(i+1).getCoef();
						Monom m1 = new Monom(coef, this.monom.get(i).getPutere());
						monom1.add(m1);
						this.monom.set(i, null);
						this.monom.set(i+1, null);
					}else {
						Monom m = new Monom(this.monom.get(i).getCoef(), this.monom.get(i).getPutere());
						monom1.add(m);
					}
				}else {
					Monom m = new Monom(this.monom.get(i).getCoef(), this.monom.get(i).getPutere());
					monom1.add(m);
				}	
				}
			}
		
		this.monom.clear();
		this.monom.addAll(monom1);
	}
	
	public Polinom suma(Polinom p) {
		ArrayList<Monom> monom1 = new ArrayList<Monom>();	
		for(Monom m: this.monom) {
			boolean gasit = false;
			int coef1 = m.getCoef();
			int putere1 = m.getPutere();
			for(int j=0;j<p.monom.size();j++) {
				if(p.monom.get(j)!=null) {
					int coef2 = p.monom.get(j).getCoef();
					int putere2 = p.monom.get(j).getPutere();
					if(putere1 == putere2) {
						int coef = coef1+coef2;
						Monom m1 = new Monom(coef, putere1);
						monom1.add(m1);
						p.monom.set(j, null);
						gasit = true;
					}
				}
			}
			if(!gasit) {
				Monom m1 = new Monom(coef1, putere1);
				monom1.add(m1);
				gasit = false;
			}
		}
		for(Monom m3: p.monom) {
			if(m3 != null) {
				Monom m = new Monom(m3.getCoef(), m3.getPutere());
				monom1.add(m);
			}
		}
		Polinom sum = new Polinom(monom1);
		return sum;
	}
	
	public Polinom deriv() {
		ArrayList<Monom> monom1 = new ArrayList<Monom>();
		for(Monom m : this.monom) {
			if(m.getPutere()>=1) {
				int coef = m.getPutere()*m.getCoef();
				int putere = m.getPutere()-1;
				Monom m1 = new Monom(coef,putere);
				monom1.add(m1);
			}else {
				Monom m1 = new Monom(0,0);
				monom1.add(m1);
			}
		}
		Polinom deriv = new Polinom(monom1);
		return deriv;
	}
	
}
