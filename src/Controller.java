package tema1.tema1;

import java.awt.event.*;
import java.util.ArrayList;

public class Controller {

	View v;
	
	public Controller() {
		v = new View();
		
		v.addSumList(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Monom> monom = new ArrayList<Monom>();
				ArrayList<Monom> monom2 = new ArrayList<Monom>();
				String[] monoame1;
				String[] monoame2;
				String txt1 ="";
				String txt2 ="";
				Polinom sum = null;
				if(v.polTxt.getText().contains("-"))
					txt1 = v.polTxt.getText().replace("-", "+-");
				else txt1 = v.polTxt.getText();
				if(v.pol2Txt.getText().contains("-"))
					txt2 = v.pol2Txt.getText().replace("-", "+-");
				else txt2 = v.pol2Txt.getText();
				monoame1 = txt1.split("\\+");
				monoame2 = txt2.split("\\+");
				for(int i=0; i<monoame1.length; i++)
				{
					Monom m = new Monom(monoame1[i]);
					monom.add(m);
				}
				for(int i=0; i<monoame2.length; i++)
				{
					Monom m = new Monom(monoame2[i]);
					monom2.add(m);
				}
				Polinom p1 = new Polinom(monom);
				Polinom p2 = new Polinom(monom2);
				p1.merge();
				p2.merge();
			//	if(p1.monom.size() >= p2.monom.size()){
				sum = p1.suma(p2);
			//	}else sum = p2.suma(p1);
				String afis = sum.afisare();
				v.rezultat.setText(afis);
			}
		});
		
		v.difSumList(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Monom> monom = new ArrayList<Monom>();
				ArrayList<Monom> monom2 = new ArrayList<Monom>();
				String[] monoame1;
				String[] monoame2;
				String txt1 ="";
				String txt2 ="";
				Polinom dif = null;
				if(v.polTxt.getText().contains("-"))
					txt1 = v.polTxt.getText().replace("-", "+-");
				else txt1 = v.polTxt.getText();
				if(v.pol2Txt.getText().contains("-"))
					txt2 = v.pol2Txt.getText().replace("-", "+-");
				else txt2 = v.pol2Txt.getText();
				monoame1 = txt1.split("\\+");
				monoame2 = txt2.split("\\+");
				for(int i=0; i<monoame1.length; i++)
				{
					Monom m = new Monom(monoame1[i]);
					monom.add(m);
				}
				for(int i=0; i<monoame2.length; i++)
				{
					//int x = -1;
					Monom m = new Monom(monoame2[i]);
					int x = m.getCoef()*-1;
					m.setCoef(x);
					monom2.add(m);
				}
				Polinom p1 = new Polinom(monom);
				Polinom p2 = new Polinom(monom2);
				p1.merge();
				p2.merge();
				dif = p1.suma(p2);
				dif.merge();
				String afis = dif.afisare();
				v.rezultat.setText(afis);
			}
		});
		
		v.prodSumList(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Monom> monom = new ArrayList<Monom>();
				ArrayList<Monom> monom2 = new ArrayList<Monom>();
				String[] monoame1;
				String[] monoame2;
				String txt1 ="";
				String txt2 ="";
				Polinom prod = null;
				if(v.polTxt.getText().contains("-"))
					txt1 = v.polTxt.getText().replace("-", "+-");
				else txt1 = v.polTxt.getText();
				if(v.pol2Txt.getText().contains("-"))
					txt2 = v.pol2Txt.getText().replace("-", "+-");
				else txt2 = v.pol2Txt.getText();
				monoame1 = txt1.split("\\+");
				monoame2 = txt2.split("\\+");
				for(int i=0; i<monoame1.length; i++){
					Monom m = new Monom(monoame1[i]);
					monom.add(m);
				}
				for(int i=0; i<monoame2.length; i++) {
					Monom m = new Monom(monoame2[i]);
					monom2.add(m);
				}
				Polinom p1 = new Polinom(monom);
				Polinom p2 = new Polinom(monom2);
				p1.merge();
				p2.merge();
				prod = p1.prod(p2);
				prod.merge();
				String afis = prod.afisare();
				v.rezultat.setText(afis);
				
			}
		});
		v.deriv1List(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Monom> monom = new ArrayList<Monom>();
				String[] monoame1;
				String txt1 ="";
				Polinom deriv = null;
				if(v.polTxt.getText().contains("-"))
					txt1 = v.polTxt.getText().replace("-", "+-");
				else txt1 = v.polTxt.getText();
				monoame1 = txt1.split("\\+");
				for(int i=0; i<monoame1.length; i++){
					Monom m = new Monom(monoame1[i]);
					monom.add(m);
				}
				Polinom p1 = new Polinom(monom);
				p1.merge();
				deriv = p1.deriv();
				String afis = deriv.afisare();
				v.rezultat.setText(afis);
				
			}
		});
		v.deriv2List(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<Monom> monom = new ArrayList<Monom>();
				String[] monoame1;
				String txt1 ="";
				Polinom deriv = null;
				if(v.pol2Txt.getText().contains("-"))
					txt1 = v.pol2Txt.getText().replace("-", "+-");
				else txt1 = v.pol2Txt.getText();
				monoame1 = txt1.split("\\+");
				for(int i=0; i<monoame1.length; i++){
					Monom m = new Monom(monoame1[i]);
					monom.add(m);
				}
				Polinom p1 = new Polinom(monom);
				p1.merge();
				deriv = p1.deriv();
				String afis = deriv.afisare();
				v.rezultat.setText(afis);
				
			}
		});
	}
}
