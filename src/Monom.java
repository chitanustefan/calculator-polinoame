package tema1.tema1;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom implements Comparable {

	
	public int coef;
	public int putere;
	
	public Monom(int coef, int putere) {
		this.coef = coef;
		this.putere = putere;
	}
	
	public Monom(String monom) {
		String[] a;
		if(!monom.matches("^[xX0-9\\^\\*\\-]*")) 
			System.out.println("Format Invalid");
		Pattern polinom = Pattern.compile("\\^");
		Matcher m = polinom.matcher(monom);
		String s = "";
		while(m.find())
			s = m.group();
		if(s.isEmpty()) {
			a = monom.split("[xX]");
			if(a.length == 0) {
				this.coef = 1;
				this.putere = 1;
			} else {
				if(!a[0].isEmpty()) {
					if(a[0].equals("-"))
						this.coef = -1;
					else this.coef = Integer.parseInt(a[0]);
				} else this.coef = 1;
				if(a[0]==monom) {
					this.putere = 0;
				}else this.putere = 1;
			}
		} else {
			a = monom.split("\\^");
			try {
				String n = "";
				for( int i=0; i<a[0].length(); i++) {
					char c = a[0].charAt(i);
					if(c=='-') n +=c;
					if(c>='0' && c<='9') n+=c;
				}
			if(n.isEmpty()) {
				this.coef = 1;
			}else if(n.equals("-")) {
				this.coef = -1;
			}else this.coef = Integer.parseInt(n);		
			this.putere = Integer.parseInt(a[1]);
			} catch(NumberFormatException e) {
					System.out.println("Format invalid");
			}
		}
	}

	public int getCoef() {
		return coef;
	}

	public void setCoef(int coef) {
		this.coef = coef;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}
	
	public String getMonom() {
		if(this.putere != 0) {
			if(this.coef==1) {
				return "x^" + this.putere;
			}
			else if(this.coef==-1) {
				return "-x^" + this.putere;
			}else {
				return this.coef + "x^" + this.putere;
			}	
		}else {
			return Integer.toString(this.coef);
		}
		
	}


	public int compareTo(Object o) {
        int compare=((Monom)o).getPutere();
        /* For Ascending order*/
        return this.putere - compare;
	}
	
	
}
