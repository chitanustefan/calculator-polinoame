package tema1.tema1;

import java.util.ArrayList;
import java.util.Arrays;
import junit.framework.TestCase;

public class Test extends TestCase {

	//@org.junit.Test
	public void test() {
		
		ArrayList<Monom> monom = new ArrayList<Monom>();
		ArrayList<Monom> monom2 = new ArrayList<Monom>();
		ArrayList<Monom> monom3 = new ArrayList<Monom>();
		
		Monom m1 = new Monom(1,4);
		Monom m2 = new Monom(2,2);
		Monom m3 = new Monom(2,0);
		
		Monom m4 = new Monom(2,4);
		Monom m5 = new Monom(1,2);
		Monom m6 = new Monom(-2,1);
		Monom m7 = new Monom(5,0);
		
		//1x^4+2x^2+2
		monom.add(m1);
		monom.add(m2);
		monom.add(m3);
		
		//2x^4+x^2-2x+5
		monom2.add(m4);
		monom2.add(m5);
		monom2.add(m6);
		monom2.add(m7);
		
		
        Polinom p1 = new Polinom(monom);
        Polinom p2 = new Polinom(monom2);
        
        Polinom deriv1 = p1.deriv();
        String rez1 = deriv1.afisare();
        String expect1 = "+4x^3+4x^1+0";
        assertEquals(expect1, rez1);
        
        Polinom deriv2 = p2.deriv();
        String rez2 = deriv2.afisare();
        String expect2 = "+8x^3+2x^1-2+0";
        assertEquals(expect2, rez2);
        
        p1.merge();
        p2.merge();
        Polinom sum = p1.suma(p2);
        sum.merge();
        String rez3 = sum.afisare();
        String expect3 = "+7-2x^1+3x^2+3x^4";
        assertEquals(expect3, rez3);
        
		Monom m8 = new Monom(-2,4);
		Monom m9 = new Monom(-1,2);
		Monom m10 = new Monom(2,1);
		Monom m11 = new Monom(-5,0);
		monom3.add(m8);
		monom3.add(m9);
		monom3.add(m10);
		monom3.add(m11);
		
        Polinom p3 = new Polinom(monom3);
        Polinom dif = p1.suma(p3);
        dif.merge();
        String rez4 = dif.afisare();
        String expect4 = "-3+2x^1+x^2-x^4";
        assertEquals(expect4, rez4);
        
    }
}
